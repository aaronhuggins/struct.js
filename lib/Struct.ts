import { StructMemberTuple } from "./StructMemberTuple.ts";
import type { StructMemberDef, StructMemberType } from "./StructMemberDef.ts";
import { StructMemberValue, trimTrailingNul } from "./StructMemberValue.ts";

export type Struct<
  T extends StructDef | StructConstructor<StructDef> = StructDef,
> =
  & {
    [K in keyof InferStructDef<T>]: StructMemberType<InferStructDef<T>[K]>;
  }
  & ArrayBufferView
  & {
    /** Copy of the Struct as an ArrayBuffer. */
    arrayBuffer(): ArrayBufferLike;
    /** Clone this Struct instance. */
    clone(): Struct<T>;
    /** Clone and truncate trailing nul bytes. */
    truncate(): ArrayBuffer;
  };
export function Struct<T extends StructDef = StructDef>(
  name: string,
  definition: T,
): StructConstructor<T> {
  if (!name?.match(LEGAL_STRUCT_NAME)) {
    throw new SyntaxError(`name "${name}" is not a legal Struct name`);
  }
  const entries = Object.entries(definition);
  const byteLength = getByteLength(entries);
  const structClass = class Struct {
    static get byteLength() {
      return byteLength;
    }
    buffer!: ArrayBufferLike;
    byteLength!: number;
    byteOffset!: number;
    constructor(
      data?: BufferSource | SharedArrayBuffer,
      byteOffset?: number,
    ) {
      let buffer: ArrayBufferLike;
      Object.defineProperty(this, "buffer", {
        enumerable: false,
        get: () => buffer,
        set: (value: ArrayBuffer) => {
          buffer = value;
        },
      });
      Object.defineProperty(this, "byteLength", {
        enumerable: false,
        get: () => byteLength,
      });
      Object.defineProperty(this, "byteOffset", {
        enumerable: false,
        get: () => byteOffset,
        set: (value: number) => {
          byteOffset = value;
        },
      });
      this.buffer = ArrayBuffer.isView(data)
        ? data.buffer
        : data ?? new ArrayBuffer(byteLength);
      this.byteOffset = ArrayBuffer.isView(data)
        ? data.byteOffset + (byteOffset ?? 0)
        : byteOffset ?? 0;
      const bytes = new Uint8Array(
        this.buffer,
        this.byteOffset,
        this.byteLength,
      );
      const view = new DataView(this.buffer, this.byteOffset, this.byteLength);
      let length = 0;
      for (const [key, memberDef] of entries) {
        const {
          getter,
          setter,
          size,
        } = StructMemberValue(memberDef, length, bytes, view);
        Object.defineProperty(this, key, {
          enumerable: true,
          get: () => getter(),
          set: (value) => setter(value),
        });
        length += size;
      }
    }

    arrayBuffer() {
      return this.buffer.slice(
        this.byteOffset,
        this.byteLength,
      );
    }

    clone() {
      return new structClass(this.arrayBuffer());
    }

    truncate() {
      const bytes = new Uint8Array(
        this.buffer,
        this.byteOffset,
        this.byteLength,
      );
      return trimTrailingNul(bytes, this.byteLength).buffer;
    }

    static fromTruncated(
      data: BufferSource | SharedArrayBuffer,
      byteOffset?: number,
      byteLength?: number,
    ) {
      const bytes = new Uint8Array(structClass.byteLength);
      const buffer = ArrayBuffer.isView(data) ? data.buffer : data;
      const input = new Uint8Array(buffer, byteOffset, byteLength);
      bytes.set(input, 0);
      return new structClass(bytes);
    }
  } as StructConstructor<T>;
  Object.defineProperty(structClass, "name", {
    configurable: true,
    get: () => name,
  });
  return structClass;
}

export type StructDef = Record<string | number, StructMemberDef>;

// deno-lint-ignore ban-types
export type StructConstructor<T extends StructDef = {}> = {
  new (
    data?: BufferSource | SharedArrayBuffer,
    byteOffset?: number,
  ): Struct<T>;
  readonly byteLength: number;
  fromTruncated(
    data: BufferSource | SharedArrayBuffer,
    byteOffset?: number,
    byteLength?: number,
  ): Struct<T>;
};

type InferStructDef<T extends StructDef | StructConstructor<StructDef>> =
  T extends StructConstructor<infer D extends StructDef> ? D : T;

const LEGAL_STRUCT_NAME = /^[A-Z_][A-Za-z0-9_]*$/gu;

function getByteLength(entries: [string, StructMemberDef][]): number {
  let length = 0;
  for (const [key, memberDef] of entries) {
    if (
      key === "buffer" || key === "byteLength" || key === "byteOffset" ||
      key === "arrayBuffer" || key === "clone" || key === "truncate"
    ) {
      throw new ReferenceError(
        `property "${key}" cannot be defined on a Struct.`,
      );
    }
    const [typeDef, specifier] = StructMemberTuple(memberDef);
    if (typeof typeDef === "string") {
      if (
        typeDef === "bigint" || typeDef === "boolean" || typeDef === "number"
      ) {
        length += specifier.endsWith("64")
          ? 8
          : specifier.endsWith("32")
          ? 4
          : specifier.endsWith("16")
          ? 2
          : 1;
      } else {
        length += specifier;
      }
    } else {
      length += typeDef.byteLength;
    }
  }
  return length;
}
