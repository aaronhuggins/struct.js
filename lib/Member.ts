export type BigIntDef<Type extends BigIntType = BigIntType> = `bigint(${Type})`;
export function bigint<Type extends BigIntType>(type: Type): BigIntDef<Type> {
  return `bigint(${type})`;
}

export type BooleanDef<Type extends BigIntType = BigIntType> =
  `boolean(${Type})`;
export function boolean<Type extends BigIntType>(type: Type): BooleanDef<Type> {
  return `boolean(${type})`;
}

export type BufferDef<Size extends number = number> = `buffer(${Size})`;
export function buffer<Size extends number>(size: Size): BufferDef<Size> {
  return `buffer(${size})`;
}

export type ByteArrayDef<Size extends number = number> = `bytearray(${Size})`;
export function bytearray<Size extends number>(size: Size): ByteArrayDef<Size> {
  return `bytearray(${size})`;
}

export type CborDef<Size extends number = number> = `cbor(${Size})`;
export function cbor<Size extends number>(size: Size): CborDef<Size> {
  return `cbor(${size})`;
}

export type DateDef = typeof date;
export const date = "date" as const;

export type JsonDef<Size extends number = number> = `json(${Size})`;
export function json<Size extends number>(size: Size): JsonDef<Size> {
  return `json(${size})`;
}

export type NumberDef<Type extends NumberType = NumberType> = `number(${Type})`;
export function number<Type extends NumberType>(type: Type): NumberDef<Type> {
  return `number(${type})`;
}

export type RandomDef<Size extends number = number> = `random(${Size})`;
export function random<Size extends number>(size: Size): RandomDef<Size> {
  return `random(${size})`;
}

export type StringDef<Size extends number = number> = `string(${Size})`;
export function string<Size extends number>(size: Size): StringDef<Size> {
  return `string(${size})`;
}

export type UuidDef = typeof uuid;
export const uuid = "uuid" as const;

type Int = `int${8 | 16 | 32}`;
type Uint = `u${Int}`;
type Float = `float${32 | 64}`;
type Big = `${"u" | ""}int${64}`;
export type NumberType = Int | Uint | Float;
export type BigIntType = NumberType | Big;
