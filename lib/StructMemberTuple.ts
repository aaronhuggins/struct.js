import type { StructMemberDef } from "./StructMemberDef.ts";

type StructMemberTuple<T extends StructMemberDef> = T extends
  `${infer P}(${infer N})`
  ? N extends `${infer R extends number}` ? [P, R] : [P, N]
  : T extends "uuid" ? ["uuid", 16]
  : T extends "date" ? ["date", 8]
  : [T];
export function StructMemberTuple<T extends StructMemberDef>(
  memberDef: T,
): StructMemberTuple<T> {
  if (typeof memberDef === "string") {
    const [name, valueRaw] = memberDef.split("(");
    if (name === "date") {
      return [name, 8] as StructMemberTuple<T>;
    } else if (name === "uuid") {
      return [name, 16] as StructMemberTuple<T>;
    } else if (name === "bigint" || name === "boolean" || name === "number") {
      const value = valueRaw.slice(0, valueRaw.length - 1);
      return [name, value] as StructMemberTuple<T>;
    } else {
      const value = valueRaw.slice(0, valueRaw.length - 1);
      const size = Number.parseFloat(value);
      return [name, size] as StructMemberTuple<T>;
    }
  }
  return [memberDef] as StructMemberTuple<T>;
}
