import type {
  BigIntDef,
  BigIntType,
  BooleanDef,
  BufferDef,
  ByteArrayDef,
  CborDef,
  DateDef,
  JsonDef,
  NumberDef,
  NumberType,
  RandomDef,
  StringDef,
  UuidDef,
} from "./Member.ts";
import { StructConstructor } from "./Struct.ts";
import type { StructMemberDef, StructMemberType } from "./StructMemberDef.ts";
import { StructMemberTuple } from "./StructMemberTuple.ts";
import { AnyObject, ObjectObserver } from "./ObjectObserver.ts";
import { parse, stringify } from "./uuid.ts";
import { CBOR } from "https://deno.land/x/cbor_redux@1.0.0/mod.ts";

export type StructMemberValue<T extends StructMemberDef> = {
  getter(): StructMemberType<T>;
  setter(value: StructMemberType<T>): void;
  size: number;
};
export function StructMemberValue<T extends StructMemberDef>(
  memberDef: T,
  position: number,
  bytes: Uint8Array,
  view: DataView,
): StructMemberValue<T> {
  const [typeDef, specifier] = StructMemberTuple(memberDef);
  switch (typeDef) {
    case "bigint": {
      return bigint(specifier, position, view) as StructMemberValue<T>;
    }
    case "boolean": {
      return boolean(specifier, position, view) as StructMemberValue<T>;
    }
    case "buffer": {
      return buffer(specifier, position, bytes) as StructMemberValue<T>;
    }
    case "bytearray": {
      return bytearray(specifier, position, bytes) as StructMemberValue<T>;
    }
    case "cbor": {
      return cbor(specifier, position, bytes) as StructMemberValue<T>;
    }
    case "date": {
      return date(specifier, position, view) as StructMemberValue<T>;
    }
    case "json": {
      return json(specifier, position, bytes) as StructMemberValue<T>;
    }
    case "number": {
      return number(specifier, position, view) as StructMemberValue<T>;
    }
    case "random": {
      return random(specifier, position, bytes) as StructMemberValue<T>;
    }
    case "string": {
      return string(specifier, position, bytes) as StructMemberValue<T>;
    }
    case "uuid": {
      return uuid(specifier, position, bytes) as StructMemberValue<T>;
    }
  }
  return struct(typeDef, position, bytes) as unknown as StructMemberValue<T>;
}

function bigint<Type extends BigIntType>(
  type: Type,
  position: number,
  view: DataView,
): StructMemberValue<BigIntDef<Type>> {
  switch (type) {
    case "int64": {
      return {
        getter: () =>
          view.getBigInt64(position) as StructMemberType<BigIntDef<Type>>,
        setter: (value) => view.setBigInt64(position, value),
        size: 8,
      };
    }
    case "uint64": {
      return {
        getter: () =>
          view.getBigUint64(position) as StructMemberType<BigIntDef<Type>>,
        setter: (value) => view.setBigUint64(position, value),
        size: 8,
      };
    }
  }
  const result = number(type, position, view);
  return {
    getter: () => BigInt(result.getter()) as StructMemberType<BigIntDef<Type>>,
    setter: (value) => result.setter(Number(value)),
    size: result.size,
  };
}

function boolean<Type extends BigIntType>(
  type: Type,
  position: number,
  view: DataView,
): StructMemberValue<BooleanDef<Type>> {
  const result = bigint(type, position, view);
  return {
    getter: () =>
      Boolean(result.getter()) as StructMemberType<BooleanDef<Type>>,
    setter: (value) =>
      result.setter(BigInt(value) as StructMemberType<BigIntDef<Type>>),
    size: result.size,
  };
}

function buffer<Size extends number>(
  size: Size,
  position: number,
  bytes: Uint8Array,
): StructMemberValue<BufferDef<Size>> {
  const length = position + size;
  return {
    getter: () =>
      bytes.slice(position, length).buffer as StructMemberType<BufferDef<Size>>,
    setter: (value): void => {
      const input = new Uint8Array(value, 0, size);
      bytes.fill(0, position, length);
      bytes.set(input, position);
    },
    size,
  };
}

function bytearray<Size extends number>(
  size: Size,
  position: number,
  bytes: Uint8Array,
): StructMemberValue<ByteArrayDef<Size>> {
  const data = new Uint8Array(bytes.buffer, bytes.byteOffset + position, size);
  return {
    getter: () => data as StructMemberType<ByteArrayDef<Size>>,
    setter: (value: Uint8Array): void => {
      value = new Uint8Array(value.buffer, value.byteOffset, size);
      data.fill(0);
      data.set(value, 0);
    },
    size,
  };
}

function cbor<Size extends number>(
  size: Size,
  position: number,
  bytes: Uint8Array,
): StructMemberValue<CborDef<Size>> {
  return serializable("cbor", size, position, bytes);
}

function date(
  size: number,
  position: number,
  view: DataView,
): StructMemberValue<DateDef> {
  return {
    getter: () => new Date(Number(view.getBigInt64(position))),
    setter: (value: Date) => {
      view.setBigInt64(position, BigInt(value.getTime()));
    },
    size,
  };
}

function json<Size extends number>(
  size: Size,
  position: number,
  bytes: Uint8Array,
): StructMemberValue<JsonDef<Size>> {
  return serializable("json", size, position, bytes);
}

function number<Type extends NumberType>(
  type: Type,
  pos: number,
  view: DataView,
): StructMemberValue<NumberDef<Type>> {
  let size = 1;
  let getter = (): number => view.getUint8(pos);
  let setter = (value: number) => view.setUint8(pos, Number(value));
  switch (type) {
    case "float32": {
      size = 4;
      getter = () => view.getFloat32(pos);
      setter = (value) => view.setFloat32(pos, Number(value));
      break;
    }
    case "float64": {
      size = 8;
      getter = () => view.getFloat64(pos);
      setter = (value) => view.setFloat64(pos, Number(value));
      break;
    }
    case "int16": {
      size = 2;
      getter = () => view.getInt16(pos);
      setter = (value) => view.setInt16(pos, Number(value));
      break;
    }
    case "int32": {
      size = 4;
      getter = () => view.getInt32(pos);
      setter = (value) => view.setInt32(pos, Number(value));
      break;
    }
    case "int8": {
      size = 1;
      getter = () => view.getInt8(pos);
      setter = (value) => view.setInt8(pos, Number(value));
      break;
    }
    case "uint16": {
      size = 2;
      getter = () => view.getUint16(pos);
      setter = (value) => view.setUint16(pos, Number(value));
      break;
    }
    case "uint32": {
      size = 4;
      getter = () => view.getUint32(pos);
      setter = (value) => view.setUint32(pos, Number(value));
      break;
    }
  }
  return {
    getter,
    setter,
    size,
  } as unknown as StructMemberValue<NumberDef<Type>>;
}

function random<Size extends number>(
  size: Size,
  position: number,
  bytes: Uint8Array,
): StructMemberValue<RandomDef<Size>> {
  const data = new Uint8Array(bytes.buffer, bytes.byteOffset + position, size);
  if (data.every((n) => n === 0)) {
    crypto.getRandomValues(data);
  }
  return {
    getter: () => data as StructMemberType<RandomDef<Size>>,
    setter: (_value): void => {
      crypto.getRandomValues(data);
    },
    size,
  };
}

function string<Size extends number>(
  size: Size,
  position: number,
  bytes: Uint8Array,
): StructMemberValue<StringDef<Size>> {
  const data = new Uint8Array(bytes.buffer, bytes.byteOffset + position, size);
  return {
    getter: () => {
      const trimmed = trimTrailingNul(data, size);
      return decoder.decode(trimmed) as StructMemberType<StringDef<Size>>;
    },
    setter: (value: string): void => {
      data.fill(0);
      data.set(encoder.encode(value).slice(0, size), 0);
    },
    size,
  };
}

function struct(
  structClass: StructConstructor,
  position: number,
  bytes: Uint8Array,
) {
  const data = new Uint8Array(
    bytes.buffer,
    bytes.byteOffset + position,
    structClass.byteLength,
  );
  let struct = new structClass(data);
  return {
    getter: () => struct,
    setter: (value: typeof struct) => {
      const input = new Uint8Array(
        value.buffer,
        value.byteOffset,
        value.byteLength,
      );
      data.fill(0);
      data.set(input, 0);
      value.buffer = struct.buffer;
      value.byteOffset = struct.byteOffset;
      struct = value;
    },
    size: structClass.byteLength,
  };
}

function uuid(
  size: number,
  position: number,
  bytes: Uint8Array,
): StructMemberValue<UuidDef> {
  const data = new Uint8Array(bytes.buffer, bytes.byteOffset + position, size);
  return {
    getter: () => stringify(data),
    setter: (value) => {
      data.fill(0);
      data.set(parse(value), 0);
    },
    size,
  };
}

const encoder = new TextEncoder();
const decoder = new TextDecoder();
export function trimTrailingNul(
  bytes: Uint8Array,
  size: number,
): Uint8Array {
  let byteLength = 0;
  for (let i = size; i > 0; i--) {
    const byte = bytes[i - 1];
    if (byte > 0) {
      byteLength = i;
      break;
    }
  }
  return bytes.slice(0, byteLength);
}

function serializable<Type extends "cbor" | "json", Size extends number>(
  type: Type,
  size: Size,
  position: number,
  bytes: Uint8Array,
): StructMemberValue<`${Type}(${Size})`> {
  const data = new Uint8Array(bytes.buffer, bytes.byteOffset + position, size);
  const serialize = (value: unknown): ArrayBuffer => {
    if (type === "cbor") {
      return CBOR.encode(value);
    }
    const json = JSON.stringify(value);
    return encoder.encode(json).buffer;
  };
  const deserialize = (): unknown => {
    const input = trimTrailingNul(data.slice(0), size);
    if (input.byteLength === 0) {
      return null;
    }
    if (type === "cbor") {
      return CBOR.decode(input.buffer);
    }
    const json = decoder.decode(input);
    return JSON.parse(json);
  };
  const onChange = () => {
    const changes = serialize(object);
    if (changes.byteLength > size) {
      // Rollback changes to stored data.
      observer.disconnect();
      observer = new ObjectObserver(onChange);
      object = deserialize();
      if (object && typeof object === "object") {
        proxy = observer.observe(object as AnyObject, { all: true });
      } else {
        proxy = object;
      }
      throw new RangeError("serialized cbor length is out of bounds");
    }
    data.fill(0);
    data.set(new Uint8Array(changes), 0);
  };
  let observer = new ObjectObserver(onChange);
  let object: unknown = null;
  let proxy: unknown = null;
  object = deserialize();
  if (object && typeof object === "object") {
    proxy = observer.observe(object as AnyObject, { all: true });
  } else {
    proxy = object;
  }
  return {
    getter: () => proxy as StructMemberType<`${Type}(${Size})`>,
    setter: (value) => {
      observer.disconnect();
      observer = new ObjectObserver(onChange);
      object = value;
      if (value && typeof value === "object") {
        proxy = observer.observe(object as AnyObject, { all: true });
      } else {
        proxy = value;
      }
      onChange();
    },
    size,
  };
}
