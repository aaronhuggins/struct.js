// deno-lint-ignore-file ban-types
import type { Struct, StructConstructor, StructDef } from "./Struct.ts";
import type {
  BigIntDef,
  BooleanDef,
  BufferDef,
  ByteArrayDef,
  CborDef,
  DateDef,
  JsonDef,
  NumberDef,
  RandomDef,
  StringDef,
  UuidDef,
} from "./Member.ts";

export type StructMemberDef =
  | BigIntDef
  | BooleanDef
  | BufferDef
  | ByteArrayDef
  | CborDef
  | DateDef
  | JsonDef
  | NumberDef
  | RandomDef
  | StringDef
  | UuidDef
  | StructConstructor<{}>;
export type StructMemberType<
  T extends StructMemberDef,
> = T extends BigIntDef ? bigint
  : T extends BooleanDef ? boolean
  : T extends BufferDef ? ArrayBuffer
  : T extends ByteArrayDef ? Uint8Array
  // deno-lint-ignore no-explicit-any
  : T extends CborDef ? any
  : T extends "date" ? Date
  // deno-lint-ignore no-explicit-any
  : T extends JsonDef ? any
  : T extends NumberDef ? number
  : T extends RandomDef ? Uint8Array
  : T extends StringDef ? string
  : T extends "uuid" ? string
  : T extends StructConstructor<infer D extends StructDef> ? Struct<D>
  : undefined;
