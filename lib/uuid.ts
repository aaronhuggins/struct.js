/* Code taken in part from https://github.com/uuidjs/uuid. */

export function parse(uuid: string): Uint8Array {
  let v;
  const bytes = new Uint8Array(16);

  // Parse ########-....-....-....-............
  bytes[0] = (v = parseInt(uuid.slice(0, 8), 16)) >>> 24;
  bytes[1] = (v >>> 16) & 0xff;
  bytes[2] = (v >>> 8) & 0xff;
  bytes[3] = v & 0xff;

  // Parse ........-####-....-....-............
  bytes[4] = (v = parseInt(uuid.slice(9, 13), 16)) >>> 8;
  bytes[5] = v & 0xff;

  // Parse ........-....-####-....-............
  bytes[6] = (v = parseInt(uuid.slice(14, 18), 16)) >>> 8;
  bytes[7] = v & 0xff;

  // Parse ........-....-....-####-............
  bytes[8] = (v = parseInt(uuid.slice(19, 23), 16)) >>> 8;
  bytes[9] = v & 0xff;

  // Parse ........-....-....-....-############
  // (Use "/" to avoid 32-bit truncation when bit-shifting high-order bytes)
  bytes[10] = ((v = parseInt(uuid.slice(24, 36), 16)) / 0x10000000000) & 0xff;
  bytes[11] = (v / 0x100000000) & 0xff;
  bytes[12] = (v >>> 24) & 0xff;
  bytes[13] = (v >>> 16) & 0xff;
  bytes[14] = (v >>> 8) & 0xff;
  bytes[15] = v & 0xff;

  return bytes;
}

export function stringify(arr: number[] | Uint8Array, offset = 0) {
  // Note: Be careful editing this code!  It's been tuned for performance
  // and works in ways you may not expect. See https://github.com/uuidjs/uuid/pull/434
  return (
    byteToHex[arr[offset + 0]] +
    byteToHex[arr[offset + 1]] +
    byteToHex[arr[offset + 2]] +
    byteToHex[arr[offset + 3]] +
    "-" +
    byteToHex[arr[offset + 4]] +
    byteToHex[arr[offset + 5]] +
    "-" +
    byteToHex[arr[offset + 6]] +
    byteToHex[arr[offset + 7]] +
    "-" +
    byteToHex[arr[offset + 8]] +
    byteToHex[arr[offset + 9]] +
    "-" +
    byteToHex[arr[offset + 10]] +
    byteToHex[arr[offset + 11]] +
    byteToHex[arr[offset + 12]] +
    byteToHex[arr[offset + 13]] +
    byteToHex[arr[offset + 14]] +
    byteToHex[arr[offset + 15]]
  );
}

/**
 * Convert array of 16 byte values to UUID string format of the form:
 * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
 */
const byteToHex: string[] = [];

for (let i = 0; i < 256; ++i) {
  byteToHex.push((i + 0x100).toString(16).slice(1));
}
