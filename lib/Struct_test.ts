import type { StructMemberDef } from "./StructMemberDef.ts";
import { Struct } from "./Struct.ts";
import {
  bigint,
  BigIntType,
  boolean,
  buffer,
  bytearray,
  cbor,
  date,
  json,
  number,
  NumberType,
  random,
  string,
  uuid,
} from "./Member.ts";
import {
  assert,
  assertNotStrictEquals,
  assertStrictEquals,
  assertThrows,
} from "https://deno.land/std@0.196.0/assert/mod.ts";
import { describe, it } from "https://deno.land/x/deno_mocha@0.3.1/mod.ts";
import { StructMemberTuple } from "./StructMemberTuple.ts";

describe("Struct", () => {
  it("should create an empty Struct class", () => {
    const name = "Empty";
    const empty = Struct(name, {});

    assertStrictEquals(empty.name, name);
    assertStrictEquals(Deno.inspect(empty), `[class ${name}]`);
    assertStrictEquals(empty.byteLength, 0);
  });
  it("should error on illegal name or disallowd Struct members", () => {
    assertThrows(() => Struct("A._", {}), SyntaxError);
    assertThrows(() => Struct("__", { buffer: "uuid" }), ReferenceError);
    assertThrows(() => Struct("__", { byteLength: "uuid" }), ReferenceError);
    assertThrows(() => Struct("__", { byteOffset: "uuid" }), ReferenceError);
    assertThrows(() => Struct("__", { arrayBuffer: "uuid" }), ReferenceError);
    assertThrows(() => Struct("__", { clone: "uuid" }), ReferenceError);
    assertThrows(() => Struct("__", { truncate: "uuid" }), ReferenceError);
  });
  it("should create a Struct class with members", () => {
    const expectedSize = 50 + 50 + 100 + 4;
    const expected = {
      author: "Jean-Luc Picard",
      book_id: 302,
      subject: "United Federation of Planets Command and Leadership",
      title: "Make It So",
    };
    type Book = Struct<typeof Book>;
    const Book = Struct("Book", {
      title: string(50),
      author: string(50),
      subject: string(100),
      book_id: number("int32"),
    });
    const book = new Book();

    assert(book instanceof Book);
    assertStrictEquals(Book.byteLength, expectedSize);
    assertStrictEquals(book.byteLength, Book.byteLength);
    assertStrictEquals(book.author, "");
    assertStrictEquals(book.book_id, 0);
    assertStrictEquals(book.subject, "");
    assertStrictEquals(book.title, "");

    book.author = expected.author;
    book.book_id = expected.book_id;
    book.subject = expected.subject;
    book.title = expected.title;

    assertStrictEquals(book.author, expected.author);
    assertStrictEquals(book.book_id, expected.book_id);
    assertStrictEquals(book.subject, expected.subject);
    assertStrictEquals(book.title, expected.title);

    const clone = book.clone();

    assert(clone instanceof Book);
    assertNotStrictEquals(clone, book);
    assertNotStrictEquals(clone.buffer, book.buffer);
    assertStrictEquals(clone.author, expected.author);
    assertStrictEquals(clone.book_id, expected.book_id);
    assertStrictEquals(clone.subject, expected.subject);
    assertStrictEquals(clone.title, expected.title);

    const json: Book = JSON.parse(JSON.stringify(book));

    assertStrictEquals(json.author, expected.author);
    assertStrictEquals(json.book_id, expected.book_id);
    assertStrictEquals(json.subject, expected.subject);
    assertStrictEquals(json.title, expected.title);
    assertStrictEquals(json.arrayBuffer, undefined);
    assertStrictEquals(json.clone, undefined);
    assertStrictEquals(json.buffer, undefined);
    assertStrictEquals(json.byteLength, undefined);
    assertStrictEquals(json.byteOffset, undefined);
  });
  it("should get and set all Struct types", () => {
    const Uuid = Struct("Uuid", { data: "uuid" });
    const keys = [
      ...bigintTypes.map(bigint),
      ...bigintTypes.map(boolean),
      buffer(8),
      bytearray(8),
      cbor(16),
      date,
      json(16),
      ...numberTypes.map(number),
      random(8),
      string(8),
      uuid,
      "struct",
    ] as const;
    const def = Object.fromEntries(keys.map((key) => {
      if (key === "struct") {
        return [key, Uuid as StructMemberDef] as const;
      }
      return [key, key as StructMemberDef] as const;
    }));
    const Everything = Struct("Everything", def);
    const everything = new Everything();
    for (const key of keys) {
      if (key !== "struct") {
        const [typeDef, spec] = StructMemberTuple(key);
        switch (typeDef) {
          case "bigint": {
            const value = everything[key];
            assert(typeof value === "bigint");
            assertStrictEquals(value, 0n);
            everything[key] = 4n;
            assertStrictEquals(everything[key], 4n);
            break;
          }
          case "boolean": {
            const value = everything[key];
            assert(typeof value === "boolean");
            assertStrictEquals(value, false);
            everything[key] = true;
            assertStrictEquals(everything[key], true);
            break;
          }
          case "buffer": {
            let value = everything[key];
            assert(value instanceof ArrayBuffer);
            assertStrictEquals(value.byteLength, spec);
            assertStrictEquals(new Uint8Array(value)[0], 0);
            everything[key] = new Uint8Array([4, 4, 4, 4, 4, 4, 4, 4]).buffer;
            value = everything[key] as ArrayBuffer;
            assertStrictEquals(new Uint8Array(value)[0], 4);
            break;
          }
          case "bytearray": {
            const value = everything[key];
            assert(value instanceof Uint8Array);
            assertStrictEquals(value.byteLength, spec);
            assertStrictEquals(value[0], 0);
            everything[key] = new Uint8Array([4, 4, 4, 4, 4, 4, 4, 4]);
            assertStrictEquals(value[0], 4);
            break;
          }
          case "cbor": {
            const value = everything[key];
            assertStrictEquals(value, null);
            everything[key] = { hello: null };
            assertStrictEquals(everything[key].hello, null);
            everything[key].hello = 4;
            assertStrictEquals(everything[key].hello, 4);
            assertThrows(() => {
              everything[key].hello = "Star Trek: The Next Generation";
            });
            assertStrictEquals(everything[key].hello, 4);
            everything[key].hello = {};
            assertStrictEquals(JSON.stringify(everything[key].hello), "{}");
            everything[key] = 18;
            assertStrictEquals(everything[key], 18);
            assertThrows(() => {
              everything[key] = "Star Trek: The Next Generation";
            });
            assertStrictEquals(everything[key], 18);
            break;
          }
          case "date": {
            const value = everything[key];
            assert(value instanceof Date);
            assertStrictEquals(value.getUTCFullYear(), 1970);
            const expected = new Date();
            everything[key] = expected;
            const actual = everything[key] as Date;
            assertStrictEquals(actual.getTime(), expected.getTime());
            break;
          }
          case "json": {
            const value = everything[key];
            assertStrictEquals(value, null);
            everything[key] = { hello: null };
            assertStrictEquals(everything[key].hello, null);
            everything[key].hello = 4;
            assertStrictEquals(everything[key].hello, 4);
            assertThrows(() => {
              everything[key].hello = "Star Trek: The Next Generation";
            });
            assertStrictEquals(everything[key].hello, 4);
            everything[key] = 18;
            assertStrictEquals(everything[key], 18);
            assertThrows(() => {
              everything[key] = "Star Trek: The Next Generation";
            });
            assertStrictEquals(everything[key], 18);
            everything[key] = {};
            assertStrictEquals(JSON.stringify(everything[key]), "{}");
            break;
          }
          case "number": {
            const value = everything[key];
            assert(typeof value === "number");
            assertStrictEquals(value, 0);
            everything[key] = 4;
            assertStrictEquals(everything[key], 4);
            break;
          }
          case "random": {
            const value = everything[key];
            assert(value instanceof Uint8Array);
            assertStrictEquals(value.byteLength, spec);
            const zero = Array.from({ length: 8 }, () => "00").join("");
            const str1 = Array.from(
              value,
              (b) => b.toString(16).padStart(2, "0"),
            ).join("");
            assertNotStrictEquals(str1, zero);
            everything[key] = new Uint8Array([4, 4, 4, 4, 4, 4, 4, 4]);
            const str2 = Array.from(
              value,
              (b) => b.toString(16).padStart(2, "0"),
            ).join("");
            assertNotStrictEquals(str1, str2);
            break;
          }
          case "string": {
            const value = everything[key];
            assert(typeof value === "string");
            assertStrictEquals(value, "");
            everything[key] = "hello, world!";
            const result = everything[key] as string;
            assertStrictEquals(result.length, spec);
            assertStrictEquals(result, "hello, w");
            break;
          }
          case "uuid": {
            const uuidv4 = crypto.randomUUID();
            const value = everything[key];
            assert(typeof value === "string");
            assertStrictEquals(value, "00000000-0000-0000-0000-000000000000");
            everything[key] = uuidv4;
            assertStrictEquals(everything[key], uuidv4);
            break;
          }
        }
      } else {
        const value = everything[key];
        assert(value instanceof Uuid);
        assertStrictEquals(value.byteLength, 16);
        const uuidv4 = crypto.randomUUID();
        const sample = new (Struct("Sample", {
          uuid: Uuid,
        }))();
        sample.uuid.data = uuidv4;
        assertStrictEquals(sample.uuid.data, uuidv4);
        sample.uuid = value;
        assertStrictEquals(sample.uuid.data, value.data);
      }
    }
    everything.clone();
  });
  it("should handle truncated data", () => {
    const Person = Struct("Person", {
      name: string(20),
      age: number("uint8"),
      favoriteSport: string(20),
    });
    const person = new Person();
    assertStrictEquals(person.truncate().byteLength, 0);
    person.name = "Jean-Luc Picard";
    assertStrictEquals(person.truncate().byteLength, 15);
    person.age = 52;
    assertStrictEquals(person.truncate().byteLength, 21);
    person.favoriteSport = "Horseback Riding";
    const truncated = person.truncate();
    assertStrictEquals(truncated.byteLength, 37);
    assertNotStrictEquals(truncated.byteLength, Person.byteLength);
    const clone1 = Person.fromTruncated(truncated);
    assertStrictEquals(clone1.name, person.name);
    assertStrictEquals(clone1.age, person.age);
    assertStrictEquals(clone1.favoriteSport, person.favoriteSport);
    const clone2 = Person.fromTruncated(
      new Uint8Array(
        person.buffer,
        person.byteOffset,
        37,
      ),
    );
    assertStrictEquals(clone2.name, person.name);
    assertStrictEquals(clone2.age, person.age);
    assertStrictEquals(clone2.favoriteSport, person.favoriteSport);
  });
});

const numberTypes = [
  "float32",
  "float64",
  "int16",
  "int32",
  "int8",
  "uint16",
  "uint32",
  "uint8",
] as const satisfies readonly NumberType[];
const bigintTypes = [
  ...numberTypes,
  "int64",
  "uint64",
] as const satisfies readonly BigIntType[];
