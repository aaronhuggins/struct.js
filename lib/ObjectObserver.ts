export class ObjectObserver {
  #connected = false;
  #listener: ObjectChangeListener;
  #proxyAndTarget = new WeakMap<AnyObject, AnyObject>();
  #targetAndProxy = new WeakMap<AnyObject, AnyObject>();
  #optionCache = new WeakSet<ObjectObserverInit>();
  #errorText(type: "target" | "options" | "construct"): string {
    const FailedConstruct = "Failed to construct 'ObjectObserver': ";
    const FailedObserve = "Failed to execute 'observe' on 'ObjectObserver': ";
    if (type === "construct") {
      return FailedConstruct + "listener is not of type 'Function'.";
    }
    if (type === "target") {
      return FailedObserve + "target is not of type 'Object' or 'Array'.";
    }
    const keys = OPTION_KEYS.map((s, i, k) =>
      i === k.length - 1 ? `or '${s}'` : `'${s}'`
    ).join(", ");
    return `${FailedObserve}The options object must set at least one of ${keys} to true.`;
  }
  #isReconfigure(
    oldDesc: PropertyDescriptor | undefined,
    newDesc: PropertyDescriptor,
  ) {
    if (oldDesc === undefined) {
      return false;
    }
    return (this.#isDataDescriptor(oldDesc) &&
      this.#isAccessorDescriptor(newDesc)) ||
      (this.#isAccessorDescriptor(oldDesc) &&
        this.#isDataDescriptor(newDesc)) ||
      (typeof newDesc.configurable === "boolean" &&
        oldDesc.configurable !== newDesc.configurable) ||
      (typeof newDesc.enumerable === "boolean" &&
        oldDesc.enumerable !== newDesc.enumerable) ||
      (typeof newDesc.writable === "boolean" &&
        oldDesc.writable !== newDesc.writable);
  }
  #isAccessorDescriptor(desc?: PropertyDescriptor) {
    return desc !== undefined &&
      (Reflect.has(desc, "get") ||
        Reflect.has(desc, "set"));
  }
  #isDataDescriptor(desc?: PropertyDescriptor) {
    return desc !== undefined &&
      (Reflect.has(desc, "value") ||
        Reflect.has(desc, "writable"));
  }
  #observe<T extends AnyObject>(target: T, options: ObjectObserverInit): T {
    if (this.#proxyAndTarget.has(target)) {
      return target;
    }
    if (this.#targetAndProxy.has(target)) {
      return this.#targetAndProxy.get(target) as T;
    }
    const proxy = new Proxy(target, {
      // deno-lint-ignore no-explicit-any
      defineProperty: (target, name: any, newDesc) => {
        let result: boolean;
        if (this.#connected) {
          const oldValue = Reflect.get(target, name);
          const oldDesc = Reflect.getOwnPropertyDescriptor(target, name);
          const type = "reconfigure";
          result = Reflect.defineProperty(target, name, newDesc);
          if (
            options[type] && result && this.#isReconfigure(oldDesc, newDesc)
          ) {
            this.#listener({
              target,
              type,
              observer: this,
              name,
              oldValue,
              oldDesc,
              newDesc,
            });
          }
        } else {
          result = Reflect.defineProperty(target, name, newDesc);
        }
        return result;
      },
      // deno-lint-ignore no-explicit-any
      deleteProperty: (target, name: any) => {
        let result: boolean;
        if (this.#connected) {
          const oldValue = Reflect.get(target, name);
          const oldDesc = Reflect.getOwnPropertyDescriptor(target, name);
          result = Reflect.deleteProperty(target, name);
          if (result && options.delete) {
            this.#listener({
              target,
              type: "delete",
              observer: this,
              name,
              oldValue,
              oldDesc,
            });
          }
        } else {
          result = Reflect.deleteProperty(target, name);
        }
        return result;
      },
      // deno-lint-ignore no-explicit-any
      get: (target, name: any, receiver) => {
        const value = Reflect.get(target, name, receiver);
        if (value && typeof value === "object") {
          return this.getProxyOf(value) ?? this.#observe(value, options);
        }
        return value;
      },
      preventExtensions: (target) => {
        const result = Reflect.preventExtensions(target);
        if (this.#connected && result && options.preventExtensions) {
          this.#listener({
            target,
            type: "preventExtensions",
            observer: this,
          });
        }
        return result;
      },
      // deno-lint-ignore no-explicit-any
      set: (target, name: any, newValue, receiver) => {
        if (newValue && typeof newValue === "object") {
          if (this.#proxyAndTarget.has(newValue)) {
            newValue = this.#proxyAndTarget.get(newValue);
          } else {
            this.#observe(newValue, options);
          }
        }
        let result: boolean;
        if (this.#connected) {
          const oldValue = Reflect.get(target, name, receiver);
          const type = Reflect.has(target, name) ? "update" : "add";
          result = Reflect.set(target, name, newValue, receiver);
          if (result && oldValue !== newValue && options[type]) {
            this.#listener({
              target,
              type,
              observer: this,
              name,
              oldValue,
              newValue,
            });
          }
        } else {
          result = Reflect.set(target, name, newValue, receiver);
        }
        return result;
      },
      setPrototypeOf: (target, newValue) => {
        let result: boolean;
        if (this.#connected) {
          const oldValue = Reflect.getPrototypeOf(target);
          result = Reflect.setPrototypeOf(target, newValue);
          if (result && oldValue !== newValue && options.setPrototype) {
            this.#listener({
              target,
              type: "setPrototype",
              observer: this,
              oldValue,
              newValue,
            });
          }
        } else {
          result = Reflect.setPrototypeOf(target, newValue);
        }
        return result;
      },
    });
    this.#proxyAndTarget.set(proxy, target);
    this.#targetAndProxy.set(target, proxy);
    return proxy;
  }

  constructor(listener: ObjectChangeListener) {
    if (typeof listener !== "function") {
      throw new TypeError(this.#errorText("construct"));
    }
    this.#listener = listener;
  }

  /**
   * Stops observer from observing any mutations. Until the observe() method
   * is used again, observer's callback will not be invoked.
   */
  disconnect() {
    this.#connected = false;
  }

  /**
   * Wraps a given target in Proxy to observe and report any changes based on
   * the criteria given by options (an object). Changes can only be observed
   * on the returned proxied object.
   *
   * The options argument allows for setting mutation observation options via
   * object members.
   */
  observe<T extends AnyObject>(target: T, options: ObjectObserverInit): T {
    if (!target || typeof target !== "object") {
      throw new TypeError(this.#errorText("target"));
    }
    if (!this.#optionCache.has(options ?? {})) {
      if (Object.values(options ?? {}).some((v) => v === true)) {
        if (options.all) {
          options = Object.fromEntries(OPTION_KEYS.map((k) => [k, true]));
          options = Object.freeze(options);
        } else {
          options = Object.freeze({ ...options });
        }
        this.#optionCache.add(options);
      } else {
        throw new TypeError(this.#errorText("options"));
      }
    }
    const proxy = this.#observe(target, options);
    this.#connected = true;
    return proxy;
  }

  /** Get the observed target of a proxy created by this observer. */
  getTargetOf<T extends AnyObject>(proxy: T): T | undefined {
    if (proxy && typeof proxy === "object" && this.#proxyAndTarget.has(proxy)) {
      return this.#proxyAndTarget.get(proxy) as T;
    }
  }

  /** Get the proxy of a target observed by this instance. */
  getProxyOf<T extends AnyObject>(target: T): T | undefined {
    if (
      target && typeof target === "object" && this.#targetAndProxy.has(target)
    ) {
      return this.#targetAndProxy.get(target) as T;
    }
  }
}

// deno-lint-ignore no-explicit-any
export type AnyObject = any[] | Record<number | string | symbol, any>;
export type ObjectChangeListener = (changes: ObjectChangeRecord) => void;
export type ObjectChangeType =
  | "add"
  | "update"
  | "delete"
  | "reconfigure"
  | "setPrototype"
  | "preventExtensions";
export type ObjectChangeRecord = {
  target: unknown;
  type: ObjectChangeType;
  observer: ObjectObserver;
  name?: number | string | symbol;
  oldValue?: unknown;
  newValue?: unknown;
  oldDesc?: PropertyDescriptor;
  newDesc?: PropertyDescriptor;
};
export type ObjectObserverInit = {
  all?: boolean;
  add?: boolean;
  update?: boolean;
  delete?: boolean;
  reconfigure?: boolean;
  setPrototype?: boolean;
  preventExtensions?: boolean;
};

const OPTION_KEYS = [
  "all",
  "add",
  "update",
  "delete",
  "reconfigure",
  "setPrototype",
  "preventExtensions",
] satisfies (ObjectChangeType | "all")[];
