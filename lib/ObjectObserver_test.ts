import { describe, it } from "https://deno.land/x/deno_mocha@0.3.1/mod.ts";
import { ObjectChangeRecord, ObjectObserver } from "./ObjectObserver.ts";
import { assertThrows } from "https://deno.land/std@0.196.0/assert/assert_throws.ts";
import { assertStrictEquals } from "https://deno.land/std@0.196.0/assert/assert_strict_equals.ts";
import { assert } from "https://deno.land/std@0.196.0/assert/assert.ts";
import { assertNotStrictEquals } from "https://deno.land/std@0.196.0/assert/assert_not_strict_equals.ts";

describe("ObjectObserver", () => {
  it("should create observer", () => {
    const observer = new ObjectObserver(() => {});
    assertThrows(() => {
      // deno-lint-ignore no-explicit-any
      observer.observe(undefined as any, undefined as any);
    });
    assertThrows(() => {
      // deno-lint-ignore no-explicit-any
      observer.observe({}, undefined as any);
    });
    assertThrows(() => {
      // deno-lint-ignore no-explicit-any
      new ObjectObserver({} as any);
    });
  });
  it("should observe object", () => {
    const expected = [
      "add",
      "update",
      "delete",
      "reconfigure",
      "setPrototype",
      "preventExtensions",
      "reconfigure",
      "reconfigure",
    ];
    for (let i = 0; i < 2; i++) {
      const changes: ObjectChangeRecord[] = [];
      const observer = new ObjectObserver((change) => {
        changes.push(change);
      });
      // deno-lint-ignore no-explicit-any
      const obj: any = observer.observe({
        foo: 0,
        bar: 1,
      }, { all: true });
      if (i === 1) {
        observer.disconnect();
      }
      obj.baz = obj;
      obj.foo = {};
      delete obj.baz;
      Object.defineProperty(obj, "foo", { writable: false });
      Object.setPrototypeOf(obj, {});
      Object.seal(obj);
      if (i === 1) {
        assertStrictEquals(changes.length, 0);
      } else {
        assertStrictEquals(changes.length, expected.length);
        for (let j = 0; j < changes.length; j++) {
          assertStrictEquals(changes[j].type, expected[j]);
        }
      }
    }
  });
  it("should reconfigure object", () => {
    const changes: ObjectChangeRecord[] = [];
    const observer = new ObjectObserver((change) => {
      changes.push(change);
    });
    const obj = observer.observe({
      val: 0,
    }, { reconfigure: true });
    Object.defineProperty(obj, "val", {
      enumerable: false,
    });
    Object.defineProperty(obj, "val", {
      writable: false,
    });
    Object.defineProperty(obj, "val", {
      get: () => "",
      set: () => {},
    });
    Object.defineProperty(obj, "val", {
      writable: true,
      value: 10,
    });
    Object.defineProperty(obj, "val", {
      configurable: false,
    });
    assertStrictEquals(changes.length, 5);
    assert(changes.every((c) => c.type === "reconfigure"));
  });
  it("should cache visited objects", () => {
    const observer = new ObjectObserver(() => {});
    const target = {
      val: 10,
      nil: null,
      sub: {},
    };
    const proxy = observer.observe(target, { all: true });
    observer.disconnect();
    assertStrictEquals(observer.getTargetOf(proxy), target);
    assertStrictEquals(observer.getProxyOf(target), proxy);
    assertStrictEquals(observer.getTargetOf(target), undefined);
    assertStrictEquals(observer.getProxyOf(proxy), undefined);
    assertStrictEquals(target.val, proxy.val);
    assertStrictEquals(target.nil, proxy.nil);
    assertNotStrictEquals(target.sub, proxy.sub);
    assertStrictEquals(observer.getTargetOf(proxy.sub), target.sub);
    assertStrictEquals(observer.getProxyOf(target.sub), proxy.sub);
    assertStrictEquals(observer.observe(target, { all: true }), proxy);
    assertStrictEquals(observer.observe(proxy, { all: true }), proxy);
  });
});
