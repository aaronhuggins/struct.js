import { Struct } from "./lib/Struct.ts";
export default Struct;
export * from "./lib/Struct.ts";
export * as Member from "./lib/Member.ts";
